///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Oze Farris <ofarris@hawaii.edu>
/// @date    25_01_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#define DEFAULT_MAX_NUMBER (2048)

int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   char playOrNot;
   int maxValue;
   int finished = 0;
   int aGuess;
   printf("If you would like to enter a integer type 'y' and enter if not type 'n' and enter\n");
   scanf("%c", &playOrNot);
   if(playOrNot == 'y'){
      // Note:  Don't enter unexpected values like 'blob' or 1.5
      printf("Enter an integer greater than 0: ");
      scanf("%d", &maxValue);
   }
   else if(playOrNot == 'n') {
      maxValue = DEFAULT_MAX_NUMBER;
      printf("LALALALA Max is %d\n", maxValue);
   }
   else {
      printf("You did not enter a valid option.  Please restart.\n");
      return 1;
   }
   if(maxValue < 1){
      printf("Error! Value entered is less than 1!\n");
      return 1;
   }
   int randInt = rand() % maxValue;
   printf("OK cat, I'm thinking of a number from 1 to [%d].  Make a guess: ", maxValue);
   // Note:  Don't enter unexpected values like 'blob' or 1.5
   do { 
      scanf("%d", &aGuess);
      if(aGuess < 1){
         printf("Error please enter a real number greater than 1\n");
         continue;
      }
      if(aGuess > randInt) {
         printf("No cat... the number I’m thinking of is smaller than [%d]\n", aGuess);
         continue;
      }
      if(aGuess < randInt) {
         printf("No cat... the number I’m thinking of is greater than [%d]\n", aGuess);
         continue;
      }
      if(aGuess == randInt) {
         printf("You got me\n");
         printf("           _       \n");
         printf("  ___ __ _| |_ ___ \n");
         printf(" / __/ _` | __/ __|\n");
         printf("| (_| (_| | |_\\__ \\\n");
         printf(" \\___\\__,_|\\__|___/\n\n");
         int finished = 1;
         break;
      }
   }
   while(finished != 1);
   return 0;
} 
